<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Models\Mission_content;
use App\User;
use DB;
use Log;
use Auth;
use App\Models\Store;
class StoreController extends Controller
{
    public function index(){
        $item = Store::all();
        return view('user.store.index',['item'=>$item]);
    }
    public function calculuspoint(Request $request){
        $store = Store::find($request->id);
        $user = User::find(Auth::user()->id);
        if($user->point >= $store->price && $store->counts > 0){
            $user->point -= $store->price;
            $store->counts -= 1;
            $user->save();
            $store->save();
        }else{
            return '購買失敗';
        }
        return $user->point;
    }
    protected function getValidateRules()
    {
        return [
            'level' => 'required',
            'point' => 'required',
            'type' => 'required',
            'title' => 'required',
            'content' => 'required',
        ];
    }
}
