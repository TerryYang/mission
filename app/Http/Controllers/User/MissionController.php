<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Models\Mission_content;
use App\User;
use DB;
use Log;
use Auth;
class MissionController extends Controller
{
    public function index(){
        $user = User::find(Auth::user()->id);
        return view('user.information.index',['item'=>$user]);
        
    }

    public function announcehistorymission(){
        $mission = Mission_content::with('receiveuser')
                                ->where('announce_id',Auth::user()->id)
                                ->orderBy('updated_at','desc')
                                ->get();
        return view('user.information.edit',['item'=>$mission]);
    }

    public function selfhistorymission(){
        $mission = Mission_content::where('receive_id',Auth::user()->id)
                                ->get();
        return view('user.information.receive',['item'=>$mission]);
    }

    public function getgift(){
        $mission = Mission_content::where('receive_id',Auth::user()->id)
                                    ->get();
        
    }

    public function mission_manage(){
        $mission = Mission_content::where('announce_id',Auth::user()->id)
                                ->where('status','可接受')
                                ->get();
        foreach($mission as $i){
            $apply = explode('、',$i->apply);
            foreach($apply as $j){
                $user = User::find($j);
                $this->user = $user->name;
            }
            $i->apply = $this->user;
        }
        return view('user.mission.manage',['item' => $mission]);
    }

    public function informationupdate(Request $request){
        $user = User::find(Auth::user()->id);
        $user->information = $request->information;
        $user->save(); 
        return '儲存成功';
    }

    public function statusupdate(Request $request,$name,$id){
    }
}