<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Models\Mission_content;
use App\User;
use DB;
use Log;
use Auth;
use Illuminate\Support\Facades\Storage;
class UserController extends Controller
{
    public function index(){
        $item = Mission_content::where('status','!=','已完成')
                                ->get();
        return view('user.mission.index',['item'=>$item]);
    }

    public function announce_form(Request $request,$id){
        $id = intval($id);
        if($id > 0){
            $item = Mission_content::find($id);

            if (empty($item)) {
                abort(404);
            }
        }
        else{
            $item = new Mission_content();
            $item->id = 0;
        }

        return view('user.mission.announce_form')->with('item',$item);
        
    }

    public function mission_detail(Request $request,$id){
        $mission = Mission_content::find($id);
        return $mission;
    }

    public function receive_request(Request $request,$id){
        /*
        $mission = Mission_content::find($id);
        //將此任務放入等待核准清單
        if($mission->apply == NULL)
            $mission->apply = Auth::user()->id;
        else{
            $apply_people = explode('、',$mission->apply);
            if(!in_array(Auth::user()->id,$apply_people))
                $mission->apply = $mission->apply.'、'.Auth::user()->id;
        }
        $mission->save();
        //將此任務加入使用者申請中清單
        $user = User::find(Auth::user()->id);
        if($user->apply == NULL)
            $user->apply = $id;
        else{
            $apply_mission = explode('、',$user->apply);
            if(!in_array($id,$apply_mission))
                $user->apply = $user->apply.'、'.$id;
        }
        $user->save();

        return response()->json(null);*/
        $user = User::find(Auth::user()->id);
        $mission = Mission_content::find($id);
        $mission->receive_id = $user->id;
        $mission->status = '執行中';
        $user->ing_counts += 1;
        $mission->save();
        $user->save();
    }

    public function cancel_check_request(Request $request,$id){
        $mission = Mission_content::find($id);
        $mission->status = '等待委託人取消';
        $mission->save();
    }

    public function cancel_request(Request $request,$id){
        $mission = Mission_content::find($id);
        $user = User::find($mission->receive_id);
        $mission->receive_id = 0;
        $mission->status = '可接受';
        $user->ing_counts -= 1;
        $user->giveup_counts += 1;
        $mission->save();
        $user->save();
    }

    public function check_request(Request $request,$id){
        $mission = Mission_content::find($id);
        $mission->status = '等待委託人確認';
        $mission->save();
    }

    public function finish_request(Request $request,$id){
        $mission = Mission_content::find($id);
        $user = User::find($mission->receive_id);
        $userusingnow = User::find(Auth::user()->id);
        $mission->status = '已完成';
        $user->finish_counts += 1;
        $user->ing_counts -= 1;
        $userusingnow->mission_counts -= 1;
        $user->point += $mission->point;
        $mission->save();
        $user->save();
    }

    public function createOrUpdate(Request $request){
        $this->validate($request, $this->getValidateRules());
        DB::beginTransaction();
        try
        {
            if ($request->id == 0) {
                $item = $this->create($request);
            }
            else {
                $item = $this->update($request);
            }
            DB::commit();
        }
        catch (Exception $ex)
        {
            DB::rollback();
            Log::error($ex);
            return redirect(url()->previous())
                ->withInput($request->except([]))
                ->with('errorMessage', $ex->getMessage());
        }

        if ($request->is_valid) {
            return redirect(route('user_home'))
                ->with('successMessage', '儲存成功');
        } 
        else {
            return redirect(route('user_home'))
                ->with('successMessage', '刪除成功');
        }
    }

    public function create(Request $request){
        $user = User::find(Auth::user()->id);
        if($request->point > $user->point){
            return ;
        }
        $item = User::find(Auth::user()->id)
        ->announcemission()
        ->create([
            'level' => $request->level,
            'type' => $request->type,
            'point' => $request->point,
            'title' => $request->title,
            'content' => $request->content,
            'status' => '可接受',
        ]);
        
        $user->mission_counts = $user->mission_counts +1;
        $user->save();
        $item->save();
        return $item;
    }

    public function update(Request $request){
        $item = Mission_content::find($request->id);
        $user = User::find(Auth::user()->id);
        if (empty($item)) {
            abort(404);
        }
        if($request->is_valid){
            if($user->point < $request->point){
                return ;
            }
            $user->point += $item->point;
            $user->point -= $request->point;
            $item->title = $request->title;
            $item->content = $request->content;
            $item->type = $request->type;
            $item->point = $request->point;
            $item->level = $request->level;
            $user->save();
            $item->save();
        } 
        else {
            $user->point += $item->point;
            $item->delete();
            $user->mission_counts = $user->mission_counts -1;
            $item->save();
            $user->save();
        }
        return $item;
    }

    public function uploadimg(Request $request){
        if($request->image){
            $imageName = date('Y-m-d').'_'. Auth::user()->account.'.'.$request->image->extension();
            $request->image->move(public_path('images'), $imageName);
            $user = User::find(Auth::user()->id);
            $user->img_url = "http://120.114.170.26/mission/public/images/".$imageName;
            $user->save();

        }
        return redirect()->back();
    }

    protected function getValidateRules()
    {
        return [
            'level' => 'required',
            'point' => 'required',
            'type' => 'required',
            'title' => 'required',
            'content' => 'required',
        ];
    }
}
