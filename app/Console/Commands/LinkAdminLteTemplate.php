<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LinkAdminLTETemplate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mission:link_template';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (file_exists(public_path('plugins/adminlte'))) {
            return $this->error('The "public/plugins/adminlte" directory already exists.');
        }
        
        if (!file_exists(public_path('plugins'))) {
            mkdir(public_path('plugins'), 755);
        }
        
        $pluginDir = base_path('vendor/almasaeed2010/adminlte/plugins');
        $handle = opendir($pluginDir);
        while ($file = readdir($handle))
        {
            if ($file === false) {
                break;
            }
            
            $dir = "{$pluginDir}/{$file}";
            $targetDir = public_path("plugins/{$file}");
            if ($file != '..' && $file != '.' && is_dir($dir) && !file_exists($targetDir))
            {
                $this->laravel->make('files')->link($dir, $targetDir);
            }
        }

        $this->laravel->make('files')->link(
            base_path('vendor/almasaeed2010/adminlte/dist'), public_path('plugins/adminlte')
        );

        $this->info('The [public/plugins/adminlte] directory has been linked.');
    }
}
