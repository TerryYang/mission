<?php

namespace App\Services;

use Route;

class UserTemplateService
{
    public function getSidebarMenu()
    {
        $sidebarMenu = [
            [
                'url' => route('user_home'),
                'name' => '任務清單',
                'icon' => 'fas fa-home',
                'navName' => 'users',
                'is_active' => false,
                'children' => null,
            ], 
            [
                'url' => '#',
                'name' => '個人紀錄',
                'icon' => 'fas fa-newspaper',
                'navName' => 'article',
                'is_active' => false,
                'children' => [
                    [
                        'url' => route('user_information'),
                        'name' => '個人小屋',
                        'is_active' => false,
                    ],
                    [
                        'url' => route('user_announce_history'),
                        'name' => '歷史發布紀錄',
                        'is_active' => false,
                    ],
                    [
                        'url' => route('user_self_history'),
                        'name' => '承接紀錄',
                        'is_active' => false,
                    ],
                ],
            ],
            [
                'url' => '#',
                'name' => '風雲榜',
                'icon' => 'fas fa-list',
                'navName' => 'article',
                'is_active' => false,
                'children' => null,
            ],
            [
                'url' => route('store'),
                'name' => '積分商店',
                'icon' => 'fas fa-balance-scale',
                'navName' => 'article',
                'is_active' => false,
                'children' => null,
            ],
            
        ];
        $currentNavName = with(Route::current())->action['navName'] ?? '';
        foreach ($sidebarMenu as $lv1 => $i)
        {
            if (is_array($i['children']))
            {
                foreach ($i['children'] as $lv2 => $j)
                {
                    if (isset($j['navName']) && $j['navName'] === $currentNavName)
                    {
                        $sidebarMenu[$lv1]['children'][$lv2]['is_active'] 
                                = $sidebarMenu[$lv1]['is_active']
                                = true;
                        break;
                    }
                }
                
                if ($sidebarMenu[$lv1]['is_active'])
                    break;
            }
            else if (isset($i['nav']) && $i['navName'] === $currentNavName)
            {
                $sidebarMenu[$lv1]['is_active'] = true;
                break;
            }
        }
        
        return $sidebarMenu;
    }
}