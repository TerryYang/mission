<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receive_appraise extends Model
{
    protected $fillable = [
        'announce_id','receive_id','appraise','level'
    ];
}
