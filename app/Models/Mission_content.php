<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Mission_content extends Model
{
    protected $fillable = [
        'announce_id','receive_id','status','level',
        'title','content','tag','type','point','apply'
    ];

    public function announceuser(){
        return $this->belongsTo(User::class,'announce_id','id');
    }

    public function receiveuser(){
        return $this->belongsTo(User::class,'receive_id','id');
    }
}
