<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAppraisesToMissionContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mission_contents', function (Blueprint $table) {
            $table->boolean('announce_appraise')->default(false)->comment('發布者評論狀態')->after('point');
            $table->boolean('receive_appraise')->default(false)->comment('接受者評論狀態')->after('announce_appraise');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mission_contents', function (Blueprint $table) {
            $table->dropColumn('announce_appraise');
            $table->dropColumn('receive_appraise');
        });
    }
}
