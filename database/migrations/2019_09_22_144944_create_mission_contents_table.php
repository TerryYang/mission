<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissionContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('announce_id')->comment('委託者');
            $table->integer('receive_id')->comment('執行者')->default('0');
            $table->string('status');
            $table->integer('level');
            $table->string('title');
            $table->text('content');
            $table->string('tag')->comment('標籤分類')->default('無');
            $table->enum('type',['point','cash'])->comment('懸賞類型');
            $table->integer('point')->comment('金額');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mission_contents', function (Blueprint $table) {
            //
        });
    }
}
