<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMissionsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('finish_counts')->after('mission_counts')->comment('已完成數量')->default('0');
            $table->integer('ing_counts')->after('finish_counts')->comment('進行中數量')->default('0');
            $table->integer('giveup_counts')->after('ing_counts')->comment('放棄數量')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('finish_counts');
            $table->dropColumn('ing_counts');
            $table->dropColumn('giveup_counts');
        });
    }
}
