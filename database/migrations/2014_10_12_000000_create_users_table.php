<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('account');
            $table->string('name');
            $table->string('development')->comment('系級');
            $table->string('email')->unique();
            $table->integer('mission_counts')->comment('任務數量')->default('0');
            $table->string('state')->comment('職位')->default('初心者');
            $table->string('appraise')->comment('評價')->default('無');
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('ban_point')->comment('警告')->default('0');
            $table->boolean('ban')->default(false);
            $table->integer('point')->comment('積分')->default('0');
            $table->text('information')->comment('自我介紹')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
