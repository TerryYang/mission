<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiveAppraisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receive_appraises', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('announce_id')->comment('委託者');
            $table->integer('receive_id')->comment('執行者');
            $table->text('appraise')->comment('評價');
            $table->integer('level')->comment('滿意度');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('receive_appraises', function (Blueprint $table) {
            //
        });
    }
}
