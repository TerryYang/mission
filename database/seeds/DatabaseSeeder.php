<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stores')->insert([
            [
                'name' =>  '數位寶寶吊飾',
                'price' => '100',
                'counts' => '10',
            ],[
                'name' =>  '數位系考古',
                'price' => '200',
                'counts' => '200',
            ],[
                'name' =>  '網頁專題作品',
                'price' => '500',
                'counts' => '2',
            ]]);
    }
}
