<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['prefix' => 'user','namespace' => 'User','middleware' => ['auth']], function(){
    Route::group(['navName' => 'mission'],function(){
        Route::get('/home', 'UserController@index')->name('user_home');
        Route::get('/home/{id}','UserController@mission_detail')->name('user_mission');
        Route::put('/home/{id}/receive', 'UserController@receive_request');
        Route::put('/home/{id}/cancel_check', 'UserController@cancel_check_request');
        Route::put('/home/{id}/cancel', 'UserController@cancel_request');
        Route::put('/home/{id}/check', 'UserController@check_request');
        Route::put('/home/{id}/finish', 'UserController@finish_request');
        Route::get('/announce_form/{id}','UserController@announce_form')->name('user_announce_form');
        Route::post('/announce_form/{id}','UserController@createOrUpdate');
        Route::post('/home/uploadimg','UserController@uploadimg');
    });
    
    Route::group(['prefix' => 'information','navName' => 'personalinformation'],function(){
        Route::get('/index','MissionController@index')->name('user_information');
        Route::get('/announce_history','MissionController@announcehistorymission')->name('user_announce_history');
        Route::get('/self_history','MissionController@selfhistorymission')->name('user_self_history');
        Route::post('/informationupdate','MissionController@informationupdate');
    });

    Route::group(['prefix' => 'mission','navName' => 'mission_manage'],function(){
        Route::get('/index','MissionController@mission_manage')->name('mission_manage');
        Route::put('/index/{name}/{id}','MissionController@statusupdate')->name('mission_accept');
        
    });
    Route::group(['prefix' => 'store','navName' => 'store'],function(){
        Route::get('/index','StoreController@index')->name('store');
        Route::post('/index/id','StoreController@calculuspoint');
    });
});
