@extends('layouts.user')
@section('header')
<style>
    .outproduct{
        width: 100%;
        text-align: center;
        display:flex;
    }
    .product{
        border: 5px solid #ccc;
        height: auto;
        width: 30%;
        margin: 20px;
        background-color: #fff;
    }
    .product img{
        border-bottom: 5px solid #ccc;
        width: 100%;
        height: 100%;
    }
    .product p{
        margin: 10px;
    }
    .product button{
        width: 100%;
        margin: 0px;
        border: 0px;
        border-radius: 0px;
        opacity: 0;
        padding: 0px;
    }
</style>
@endsection
@section('content')
<div class="outproduct">
    <div class="product">
        <div>
            <img src={{ asset('images/store/product.jpg') }}>
        </div>
        <div>
            <p>數位寶寶吊飾</p>
            <p>價格:100點</p>
            <p>剩餘數量:{{ $item[0]->counts }}</p>
        </div>
        <div>
            <button type="button" class="btn-success pro1" title="確定消耗100點兌換?" num = '1'>兌換</button>
        </div>
    </div>
    <div class="product">
        <div>
            <img src={{ asset('images/store/1.png') }}>
        </div>
        <div>
            <p>數位系考古</p>
            <p>價格:200點</p>
            <p>剩餘數量:{{ $item[1]->counts }}</p>
        </div>
        <div>
            <button type="button" class="btn-success pro2" title="確定消耗200點兌換?" num = '2'>兌換</button>
        </div>
    </div>
    <div class="product">
        <div>
            <img src={{ asset('images/store/js.png') }}>
        </div>
        <div>
            <p>網頁專題作品</p>
            <p>價格:500點</p>
            <p>剩餘數量:{{ $item[2]->counts }}</p>
        </div>
        <div>
            <button type="button" class="btn-success pro3" title="確定消耗500點兌換?" num = '3'>兌換</button>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(".product").hover(function(){
        $(this).css("border-color","#38c172");
        $(this).find('img').css("border-color","#38c172");
        $(this).find('button').css('opacity','1');
    },function(){
        $(this).css("border-color","#ccc");
        $(this).find('img').css("border-color","#ccc");
        $(this).find('button').css('opacity','0');
    });
    $(document).on('click', '.pro1', function(){
        if (!confirm($(this).attr('title'))){
            return;
        }
        $.ajax({
                url: 'http:///120.114.170.26/mission/public/user/store/index/id',
                method: 'POST',
                headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').prop('content')},
                data: {
                    'id':$(this).attr('num'),
                },
                error: function(xhr) {
                    alert("購買失敗");
                },
                success: function(response) {
                    if (response!="購買失敗"){
                        alert("購買成功! 你剩下" + response + "點");
                        location.reload();
                    }
                    else{
                        alert("購買失敗! 點數不足或商品剩餘數量不足");
                    }
                }
                });
    });
    $(document).on('click', '.pro2', function(){
        if (!confirm($(this).attr('title'))){
            return;
        }
        $.ajax({
                url: 'http:///120.114.170.26/mission/public/user/store/index/id',
                method: 'POST',
                headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').prop('content')},
                data: {
                    'id':$(this).attr('num'),
                },
                error: function(xhr) {
                    alert("購買失敗");
                },
                success: function(response) {
                    if (response!="購買失敗"){
                        alert("購買成功! 你剩下" + response + "點");
                        location.reload();
                    }
                    else{
                        alert("購買失敗! 點數不足或商品剩餘數量不足");
                    }
                }
                });
    });
    $(document).on('click', '.pro3', function(){
        if (!confirm($(this).attr('title'))){
            return;
        }
        $.ajax({
                url: 'http:///120.114.170.26/mission/public/user/store/index/id',
                method: 'POST',
                headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').prop('content')},
                data: {
                    'id':$(this).attr('num'),
                },
                error: function(xhr) {
                    alert("購買失敗");
                },
                success: function(response) {
                    if (response!="購買失敗"){
                        alert("購買成功! 你剩下" + response + "點");
                        location.reload();
                    }
                    else{
                        alert("購買失敗! 點數不足或商品剩餘數量不足");
                    }
                }
                });
    });
</script>
@endsection