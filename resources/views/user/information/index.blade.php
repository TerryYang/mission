@extends('layouts.user')
@section('header')
    <style>
        .information_block{
            margin-left: 15px;
        }
        .clear{
            clear: both;
        }
        .banner{
            padding-top:10%;
        }
        .open_btn:hover {   
            color:blue;
            cursor: pointer;
        }
        
        #background {
            display: none;
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0.5);
        }
        
        #div1 {
            border-radius: .5rem;
            background:#eeeeee;
            width: 70%;
            z-index: 1;
            margin: 8% auto;
            overflow: auto;
        }
        
        span {
            color: white;
            padding-top: 12px;
            cursor: pointer;
            padding-right: 15px;
        }
        
        #div2 {
            background:#eeeeee;
            margin: auto;
            height: 400px;
            padding: 0 20px;
        }
        #div2 > p{
            margin-top:20px;
        }
        #close {
            padding: 5px;
            background: rgba(23,162,184,0.5) ;
        }
        
        #close-button {
            float: right;
            font-size: 30px;
            line-height: 2rem;
        }
        
        h2 {
            margin: 10px 0;
            color: black;
            padding-left: 15px;
        }
        #save{
            color: black;
        }
    </style>
@endsection
@section('content')
<div class="container-fluid">
    <div>
        <div>
            <img class="float-left photo" src="{{$item->img_url}}" width="240px" height="300px">
                
        </div>
        <div>
            <div class="float-left align-top information_block">
                <label>姓名: {{ $item->name }}</label><br>
                <label>系級: {{ $item->development }}</label><br>
                <label>職位: {{ $item->state }}</label><br>
                <label>積分: {{ $item->point }}</label><br>
                <label>完成任務數: {{ $item->finish_counts }}</label><br>
                <label>放棄任務數: {{ $item->giveup_counts }}</label><br>
                <label>執行中任務: {{ $item->ing_counts }}</label><br>
                <label>發布任務數: {{ $item->mission_counts }}</label><br>
                <label>評價: {{ $item->appraise }}</label>
            </div>
            <div class="text-center banner">
                <button class="btn btn-lg btn-danger ad">觀看廣告，領取獎勵</button>
            </div>
        </div>
        
    </div>
    <div class="clear">
        <form action="http://120.114.170.26/mission/public/user/home/uploadimg" enctype="multipart/form-data" method="POST">
        @csrf
        <input type="file" name="image" id="progressbarTWInput" accept="image/gif, image/jpeg, image/png" style="width:192px">
        <input type="submit">
        </form>
        <br>
        <form action="" class="">
            <label class="">自我介紹:<span id="save"></span></label>
            <textarea class="form-control" id="information" cols="30" rows="10">{{$item->information}}</textarea>
        </form>
    </div>
    <br>
</div>
<div id="background" class="back">
    <div id="div1">
        <div id="close">
            <span id="close-button">×</span>
            <h2 class="box_title"></h2>
        </div>
        <div id="div2">
            <p class="box_content"></p>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
        $(document).ready(function(){
            $(".form-control").blur(function (){
                $.ajax({
                url: 'http://120.114.170.26/mission/public/user/information/informationupdate',
                method: 'POST',
                headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').prop('content')},
                data: {
                    information:$('#information').val()
                },
                error: function(xhr) {
                    document.getElementById("save").innerText="儲存失敗";
                },
                success: function(response) {
                    document.getElementById("save").innerText=response;
                    $("#save").fadeIn("slow");
                    $("#save").fadeOut("slow");
            
                }
                });
            });
            $("#progressbarTWInput").change(function(){
                readURL(this);
            });
            function readURL(input){
                if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(".photo").attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                }
            }
        });
    </script>
@endsection