@extends('layouts.user')
@section('header')
    <style>
        input{
            font-size: .8rem;
            border-radius: .2rem;
            border:1px solid #ced4da;
            font-weight: normal;
            margin: 0 .5rem;
            padding: .15rem .1rem;
            background: rgba(0, 0, 0, 0);
        }
        .order-sm-last{
            margin: 0;
        }
        .mg-bo{
            margin-bottom: 5px !important;
        }
        .open_btn:hover {   
            color:blue;
            cursor: pointer;
        }
        
        #background {
            display: none;
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0.5);
        }
        
        #div1 {
            border-radius: .5rem;
            background:#eeeeee;
            width: 70%;
            z-index: 1;
            margin: 8% auto;
            overflow: auto;
        }
        
        span {
            color: white;
            padding-top: 12px;
            cursor: pointer;
            padding-right: 15px;
        }
        
        #div2 {
            background:#eeeeee;
            margin: auto;
            height: 400px;
            padding: 0 20px;
        }
        #div2 > p{
            margin-top:20px;
        }
        #close {
            padding: 5px;
            background: rgba(23,162,184,0.5);
        }
        
        #close-button {
            float: right;
            font-size: 30px;
            line-height: 2rem;
        }
        
        h2 {
            margin: 10px 0;
            color: black;
            padding-left: 15px;
        }
    </style>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <table id="dataTable" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center">懸賞時間</th>
                            <th class="text-center no-sort">懸賞標題</th>
                            <th class="text-center">懸賞類型</th>
                            <th class="text-center">懸賞金額</th>
                            <th class="text-center no-sort">申請人</th>
                            <th class="text-center">懸賞狀態</th>
                            <th class="text-center no-sort no-search">操作</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($item as $i)
                            <tr>
                                <td class="text-center">{{date("Y-m-d H:i",strtotime($i->updated_at))}}</td>
                                <td class="text-center">{{ $i->title }}</td>
                                <td class="text-center">{{ $i->type }}</td>
                                <td class="text-center">{{ $i->point }}</td>
                                <td class="text-center">@if(isset($i->receiveuser->name)){{$i->receiveuser->name}} @endif</td>
                                <td class="text-center">{{ $i->status }}</td>
                                <td class="text-center">
                                    @if ($i->status == '可接受')
                                        <a href="#" link="{{ route('user_announce_form',$i->id) }}" class="btn-edit">編輯</a>
                                    @elseif ($i->status == '等待委託人確認')
                                        <a href="#" link="{{ route('user_mission',$i->id) }}" class="btn-finish" title="確認委託已完成?">確認完成</a>
                                    @elseif ($i->status == '等待委託人取消')
                                        <a href="#" link="{{ route('user_mission',$i->id) }}" class="btn-cancel" title="確認取消?">同意取消</a>
                                    @endif
                                        <a class="open_btn" link="{{ route('user_mission',$i->id) }}" style="color:rgb(52,144,220);">檢視</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div id="background" class="back">
    <div id="div1">
        <div id="close">
            <span id="close-button">×</span>
            <h2 class="box_title"></h2>
        </div>
        <div id="div2">
            <p class="box_content"></p>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function() { 
        $('#dataTable').DataTable({
            language: { url: "{{ asset('js/dataTables.zh.json') }}" },
            order: [[0, 'desc']],
            columnDefs: [{
                targets: 'no-sort',
                orderable: false
            },{
                "targets": 'no-search',
                "searchable": false
            }],
            initComplete: function(){
            }
        });
    });
    $(document).on('click', '.btn-finish', function(){
        if ($('#dataTable').data('processing') || !confirm($(this).attr('title')))
            return;
        
        $('#dataTable').data('processing', true);
        $.ajax({
            url: $(this).attr('link') + '/finish',
            method: 'PUT',
            headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').prop('content')}
        }).always(function(resp, status){
            if (status === 'success') {
                window.location.reload();
            }
            else {
                App.ajaxErrorToast(resp);
                App.ajaxErrorToast(resp);
            }
        });
    });
    $(document).on('click', '.btn-cancel', function(){
        if ($('#dataTable').data('processing') || !confirm($(this).attr('title')))
            return;
        
        $('#dataTable').data('processing', true);
        $.ajax({
            url: $(this).attr('link') + '/cancel',
            method: 'PUT',
            headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').prop('content')}
        }).always(function(resp, status){
            if (status === 'success') {
                window.location.reload();
            }
            else {
                App.ajaxErrorToast(resp);
                App.ajaxErrorToast(resp);
            }
        });
    });
    $('.open_btn').click(function(){
        $('#background').css('display','block');
    });
    $('#close_button').click(function(){
        $('.open_btn').css('display','none');
        $('.box_title').text("");
        $('.box_content').text("");
    });
    $('#background').click(function(){
        $('#background').css('display','none');
        $('.box_title').text("");
        $('.box_content').text("");
    });
    $('.open_btn').on('click', function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('link'),
            method: 'GET',
        }).always(function(resp, status){
            if (status === 'success') {
                $('.box_title').text(resp['title']);
                $('.box_content').html('<label>難度: </label>' + resp['level'] + '<br><label>積分: </label>' + resp['point'] + '<h6><b>內容:</b><br></h6>' + resp['content']);
            }
            else {
                console.log(resp);
            }
        });
    });
</script>   
@endsection