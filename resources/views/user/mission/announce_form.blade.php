@extends('layouts.user',['pageTitle' => $item->id === 0 ? '添加懸賞' : '編輯懸賞'])
@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
    <li class="breadcrumb-item"><a href="#">懸賞管理</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('user_home') }}" class="back-to-list">懸賞列表</a></li>
</ol>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <form class="card form-horizontal" action="#" method="POST">
            @csrf
            <div class="card-body">

                <div class="form-group">
                    <label>委託人:</label>
                    <label class="form-control">{{ Auth::user()->name }}</label>
                </div>

                <div class="form-group">
                    <label>委託人等級</label>
                    <label class="form-control">{{ Auth::user()->state }}</label>
                </div>

                <div class="form-group">
                    <label>懸賞等級</label>
                    <input type="number" name="level" value="{{ old('level', $item->level) }}" class="form-control" required maxlength="255" autocomplete="off" min=0 max=5>
                    <small class="form-error-message">{{ $errors->has('level') ? $errors->first('level') : '' }}</small>
                </div>

                <div class="form-group">
                    <label>懸賞類型</label>
                    <select name="type" required>
                        <option value="point">積分</option>
                        <!--<option value="cash">現金</option>-->
                    </select>
                </div>

                <div class="form-group">
                    <label>懸賞金額</label>
                    <input type="number" name="point" value="{{ old('point', $item->level) }}" class="form-control" required maxlength="255" autocomplete="off" min=1 max=1000>
                    <small class="form-error-message">{{ $errors->has('point') ? $errors->first('point') : '' }}</small>
                </div>

                <div class="form-group">
                    <label>標題</label>
                    <input type="text" name="title" value="{{ old('point', $item->title) }}" class="form-control" required  autocomplete="off">
                    <small class="form-error-message">{{ $errors->has('title') ? $errors->first('title') : '' }}</small>
                </div>

                <div class="form-group">
                    <label>內容</label>
                    <textarea name="content" class="form-control" cols="30" rows="10">{{$item->content}}</textarea>
                </div>
            </div>
            <div class="card-footer text-right">
                <input type="hidden" name="id" value="{{ $item->id }}">
                <input type="hidden" name="is_valid" value="1">
                <button type="submit" class="btn btn-primary float-right">儲存變更</button>
                <button type="button" class="btn btn-reset btn-outline-secondary mr-1">取消</button>
            </div>
        </form>
    </div>
</div>
@endsection