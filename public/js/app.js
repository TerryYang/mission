var App = new function ()
{
    const TOAST = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    
    function init() 
    {
        $(document).ready(function () 
        {
            $(document).on('keyup keypress', 'input', function (e) {
                var keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    e.stopPropagation();
                }
            });
            
            $('form').on('click', '.btn-delete', function () {
                if (confirm('確認要刪除?')) {
                    $('[name=is_valid]').val(0);
                    $(this).parents('form').submit();
                }
            }).on('click', '.btn-reset', function () {
                var url = $('a.back-to-list').length > 0 ? $('a.back-to-list').prop('href') : location.href;
                location.href = url;
            });
            
            initDateTimePicker();
            
            $('.toast-message').each(function(i, msg){
                TOAST.fire({
                    type: $(msg).data('type'),
                    title: $(msg).text()
                });
            });
        });
    }
    
    function ajaxErrorToast(resp)
    {
        var errorMessage = 'Unknown Error.';
        if (resp.responseJSON['errors']) {
            errorMessage = Object.keys(resp.responseJSON['errors']).map(function(i){ return (typeof(resp.responseJSON['errors'][i]) === 'string' ? resp.responseJSON['errors'][i] : resp.responseJSON['errors'][i][0]); }).join("\n");
        }
        else if (resp.responseJSON['message']) {
            errorMessage = resp.responseJSON['message'];
        }

        TOAST.fire({
            type: 'error',
            title: errorMessage
        });
    }
    
    
    function initDateTimePicker()
    {
        if (typeof($.fn.daterangepicker) === 'undefined')
            return;
        
        $('.datetimepicker-form-control').daterangepicker({
            timePicker: true,
            singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                format: 'YYYY-MM-DD HH:mm:ss',
                cancelLabel: '清除',
                applyLabel: '應用'
            }
        }).on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format(picker.locale.format));
        }).on('cancel.daterangepicker', function() {
            $(this).val('');
        });
    }
    
    return {
        init: init,
        ajaxErrorToast: ajaxErrorToast,
    };
}